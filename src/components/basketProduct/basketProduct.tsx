import { FC } from 'react';
import { Link } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import { Stack } from '@mui/material';
import Typography from '@mui/material/Typography';
import Price from '../price';
import DiscountChip from '../discountСhip';
import QuantityButton from '../quantityButton';
import { TProduct } from '../../models/models';
import { useAppSelector } from '../../store/hooks';
import { selectUser } from '../../store/reducers/root-layout/selectors';
import Like from '../like';

export type TProductCardProps = {
	product: TProduct;
};

export const BasketProduct: FC<TProductCardProps> = ({ product }) => {
	const user = useAppSelector(selectUser);

	return (
		<Card sx={{ display: 'flex', height: '15rem' }}>
			<Link to={`/product/${product.id}`}>
				<CardMedia
					component='img'
					alt={product.name}
					image={product.pictures}
					sx={{ width: '12rem', height: '15rem' }}
				/>
			</Link>
			<CardContent sx={{ width: '100%' }}>
				<Stack direction={'column'} spacing={2}>
					<Stack direction={'row'} spacing={2}>
						<Price price={product.price} discount={product.discount} />
						<DiscountChip price={product.price} discount={product.discount} />
					</Stack>
					<Typography variant='subtitle2' fontWeight='light'>
						`в наличии {product.stock} шт`
					</Typography>
					<Typography variant='h6'>{product.name}</Typography>
					<Stack direction={'row'} spacing={2}>
						<Like
							isLike={product.likes.includes(user?.id as string)}
							productId={product.id}
						/>
						<QuantityButton productId={product.id} stock={product.stock} />
					</Stack>
				</Stack>
			</CardContent>
		</Card>
	);
};
