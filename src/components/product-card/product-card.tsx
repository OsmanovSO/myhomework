import {
	Avatar,
	Card,
	CardContent,
	CardHeader,
	Typography,
} from '@mui/material';
import CardActions from '@mui/material/CardActions';
import { Link } from 'react-router-dom';
import { FC } from 'react';
import { red } from '@mui/material/colors';
import CardMedia from '@mui/material/CardMedia';
import styles from './product-card.module.css';
import Like from '../like';
import { useAppSelector } from '../../store/hooks';
import { TProduct } from '../../models/models';
import { selectUser } from '../../store/reducers/root-layout/selectors';
import { withQuery } from '../../HOCs/with-query';
import { QuantityButton } from '../quantityButton/quantityButton';

export const ProductCard: FC<
	TProduct & {
		isLoading: boolean;
		isError: boolean;
		error?: string;
		refetch?: () => void;
	}
> = withQuery((props) => {
	const user = useAppSelector(selectUser);
	const {
		id,
		price,
		discount,
		description,
		name,
		pictures,
		likes,
		stock,
	}: TProduct = props;

	return (
		<Card sx={{ maxWidth: 300, maxHeight: 400, minHeight: 400, margin: 2 }}>
			<CardHeader
				avatar={
					<Avatar
						sx={{ bgcolor: red[500], width: 60, height: 24, borderRadius: 25 }}
						aria-label='recipe'
						variant='square'>
						{discount}%
					</Avatar>
				}
				action={
					<Like isLike={likes.includes(user?.id as string)} productId={id} />
				}
			/>
			<Link to={`/product/${id}`}>
				<CardMedia
					image={pictures ? pictures : 'https://picsum.photos/480/320/'}
					sx={{
						height: 140,
						width: 120,
						mx: 'auto',
					}}
				/>
				<CardContent>
					<Typography className={styles.text}>{price} Р</Typography>
					<Typography noWrap component={'div'} className={styles.text}>
						{name}
					</Typography>
					<Typography noWrap variant='body2'>
						{description}
					</Typography>
				</CardContent>
			</Link>

			<CardActions>
				<QuantityButton productId={id} stock={stock} />
			</CardActions>
		</Card>
	);
});
