import React, { FC } from 'react';
import {
	AppBar,
	Container,
	Toolbar,
	Box,
	IconButton,
	Menu,
	MenuItem,
	Badge,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import FavoriteIcon from '@mui/icons-material/Favorite';
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Logout from '@mui/icons-material/Logout';
import Login from '@mui/icons-material/Login';
import { useLocation, useNavigate } from 'react-router-dom';
import { ReactComponent as LogoPage } from '../../logo/logoPage.svg';
import { Search } from '../search/search';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { selectTokens } from '../../store/reducers/root-layout/selectors';
import { setTokens } from '../../store/reducers/root-layout/root-layout-slice';
import { selectBasketCount } from '../../store/reducers/basket/selectors';

export const Header: FC = () => {
	const location = useLocation();
	const navigate = useNavigate();
	const dispatch = useAppDispatch();
	const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
		null
	);
	const basketCount = useAppSelector(selectBasketCount);
	const { accessToken } = useAppSelector(selectTokens);

	const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorElNav(event.currentTarget);
	};

	const handleCloseNavMenu = (event: React.MouseEvent<HTMLElement>) => {
		event.currentTarget.tagName !== 'LI' && setAnchorElNav(null);
	};

	const handleHome = () => {
		navigate('/');
		setAnchorElNav(null);
	};
	const handleFavourites = () => {
		navigate('/favourites');
		setAnchorElNav(null);
	};
	const handleBaskets = () => {
		navigate('/baskets');
		setAnchorElNav(null);
	};
	const handleProfile = () => {
		navigate('/profile');
		setAnchorElNav(null);
	};
	const handleLogout = () => {
		setAnchorElNav(null);
		dispatch(setTokens({ accessToken: '', refreshToken: '' }));
		navigate('/signin', { state: { from: location.pathname } });
	};
	return (
		<AppBar sx={{ backgroundColor: '#FFE44D' }}>
			<Container maxWidth='xl'>
				<Toolbar disableGutters>
					<IconButton
						onClick={handleHome}
						size='small'
						edge='start'
						color='inherit'
						aria-label='open drawer'
						sx={{ mr: 2 }}>
						<LogoPage />
					</IconButton>
					<Box sx={{ flexGrow: 1 }} />
					<Search />
					<Box sx={{ flexGrow: 1 }} />
					{accessToken ? (
						<Box sx={{ flexGrow: 0, display: { xs: 'none', md: 'flex' } }}>
							<MenuItem onClick={handleCloseNavMenu}>
								<IconButton
									onClick={handleFavourites}
									size='large'
									aria-label='account of current user'
									aria-controls='primary-search-account-menu'
									aria-haspopup='true'
									color='inherit'>
									<FavoriteIcon />
								</IconButton>
							</MenuItem>
							<MenuItem onClick={handleCloseNavMenu}>
								<IconButton
									onClick={handleBaskets}
									size='large'
									aria-label='account of current user'
									aria-controls='primary-search-account-menu'
									aria-haspopup='true'
									color='inherit'>
									<Badge badgeContent={basketCount} color='error'>
										<CardGiftcardIcon />
									</Badge>
								</IconButton>
							</MenuItem>
							<MenuItem onClick={handleCloseNavMenu}>
								<IconButton
									onClick={handleProfile}
									size='large'
									aria-label='account of current user'
									aria-controls='primary-search-account-menu'
									aria-haspopup='true'
									color='inherit'>
									<AccountCircleIcon />
								</IconButton>
							</MenuItem>
							<MenuItem onClick={handleCloseNavMenu}>
								<IconButton
									onClick={handleLogout}
									size='large'
									aria-label='account of current user'
									aria-controls='primary-search-account-menu'
									aria-haspopup='true'
									color='inherit'>
									<Logout />
								</IconButton>
							</MenuItem>
						</Box>
					) : (
						<Box sx={{ flexGrow: 0, display: { xs: 'none', md: 'flex' } }}>
							<MenuItem onClick={handleCloseNavMenu}>
								<IconButton
									onClick={handleLogout}
									size='large'
									aria-label='account of current user'
									aria-controls='primary-search-account-menu'
									aria-haspopup='true'
									color='inherit'>
									<Login />
								</IconButton>
							</MenuItem>
						</Box>
					)}
					<Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
						<IconButton
							size='large'
							aria-label='account of current user'
							aria-controls='menu-appbar'
							aria-haspopup='true'
							onClick={handleOpenNavMenu}
							color='inherit'>
							<MenuIcon />
						</IconButton>
						<Menu
							id='menu-appbar'
							anchorEl={anchorElNav}
							anchorOrigin={{
								vertical: 'bottom',
								horizontal: 'left',
							}}
							keepMounted
							transformOrigin={{
								vertical: 'top',
								horizontal: 'left',
							}}
							open={Boolean(anchorElNav)}
							onClose={handleCloseNavMenu}
							sx={{
								display: { xs: 'block', md: 'none' },
							}}>
							{accessToken ? (
								<>
									<MenuItem onClick={handleCloseNavMenu}>
										<IconButton
											onClick={handleFavourites}
											size='large'
											aria-label='account of current user'
											aria-controls='primary-search-account-menu'
											aria-haspopup='true'
											color='inherit'>
											<FavoriteIcon />
										</IconButton>
									</MenuItem>
									<MenuItem onClick={handleCloseNavMenu}>
										<IconButton
											onClick={handleBaskets}
											size='large'
											aria-label='account of current user'
											aria-controls='primary-search-account-menu'
											aria-haspopup='true'
											color='inherit'>
											<CardGiftcardIcon />
										</IconButton>
									</MenuItem>
									<MenuItem onClick={handleCloseNavMenu}>
										<IconButton
											onClick={handleProfile}
											size='large'
											aria-label='account of current user'
											aria-controls='primary-search-account-menu'
											aria-haspopup='true'
											color='inherit'>
											<AccountCircleIcon />
										</IconButton>
									</MenuItem>
									<MenuItem onClick={handleCloseNavMenu}>
										<IconButton
											onClick={handleLogout}
											size='large'
											aria-label='account of current user'
											aria-controls='primary-search-account-menu'
											aria-haspopup='true'
											color='inherit'>
											<Logout />
										</IconButton>
									</MenuItem>
								</>
							) : (
								<MenuItem onClick={handleCloseNavMenu}>
									<IconButton
										onClick={handleLogout}
										size='large'
										aria-label='account of current user'
										aria-controls='primary-search-account-menu'
										aria-haspopup='true'
										color='inherit'>
										<Login />
									</IconButton>
								</MenuItem>
							)}
						</Menu>
					</Box>
				</Toolbar>
			</Container>
		</AppBar>
	);
};
