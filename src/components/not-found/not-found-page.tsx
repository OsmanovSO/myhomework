import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import { Paper, Typography } from '@mui/material';
import { Box } from '@mui/system';

const NotFoundPage = () => {
	return (
		<Box
			display='flex'
			justifyContent='center'
			alignItems='center'
			minHeight='80vh'>
			<Paper elevation={3} sx={{ padding: 3, textAlign: 'center' }}>
				<Typography variant='h4' gutterBottom>
					404 - Страница не найдена
				</Typography>
				<Typography variant='body1' paragraph>
					К сожалению, запрашиваемая вами страница не существует.
				</Typography>
				<Button component={Link} to='/' variant='contained' color='primary'>
					Вернуться на главную
				</Button>
			</Paper>
		</Box>
	);
};

export default NotFoundPage;
