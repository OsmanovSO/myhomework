import React from 'react';
import {
	Typography,
	Link,
	Grid,
	Toolbar,
	List,
	ListItem,
	Stack,
	AppBar,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Facebook, Twitter, Instagram, LinkedIn } from '@mui/icons-material';

import { ReactComponent as LogoPage } from '../../logo/logoPage.svg';

const useStyles = makeStyles(() => ({
	listItem: {
		marginBottom: '0px',
	},
	gridContainer: {
		padding: '0',
	},
	gridItem: {
		padding: '0',
	},
	iconContainer: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
}));

const navElements = [
	{ title: 'Каталог', url: '/catalog' },
	{ title: 'Акции', url: '/promotions' },
	{ title: 'Новости', url: '/news' },
	{ title: 'Отзывы', url: '/reviews' },
];

const additionalNavElements = [
	{ title: 'Оплата и доставка', url: '/payment' },
	{ title: 'Часто спрашивают', url: '/faq' },
	{ title: 'Обратная связь', url: '/feedback' },
	{ title: 'Контакты', url: '/contacts' },
];

const socialMedia = [
	{ type: Facebook, url: 'https://facebook.com' },
	{ type: Twitter, url: 'https://twitter.com' },
	{ type: Instagram, url: 'https://instagram.com' },
	{ type: LinkedIn, url: 'https://linkedin.com' },
];

function IconWithDescription({
	type,
	description,
}: {
	type: string;
	description: string;
}) {
	const classes = useStyles();

	return (
		<>
			{type === 'facebook' && (
				<Link width={1} href={`/${description}`} sx={{ color: 'black' }}>
					<div className={classes.iconContainer}>
						<Facebook />
						<Typography variant='caption' align='center'>
							{description}
						</Typography>
					</div>
				</Link>
			)}
		</>
	);
}

export const Footer: React.FC = () => {
	const classes = useStyles();

	return (
		<AppBar
			component='footer'
			sx={{ backgroundColor: '#FFE44D', mt: 'auto' }}
			position='static'>
			<Toolbar disableGutters>
				<Grid
					container
					sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}
					spacing={2}
					className={classes.gridContainer}>
					<Grid
						item
						xs={12}
						sm={6}
						md={3}
						flexDirection={'column'}
						justifyContent={'center'}
						alignItems={'center'}
						className={classes.gridItem}
						sx={{
							textAlign: 'center',
							color: 'black',
							display: 'flex',
						}}>
						<LogoPage />
						<Typography
							sx={{ marginTop: '20px', color: 'black' }}
							variant='body2'>
							© «Интернет-магазин DogFood.ru»
						</Typography>
					</Grid>
					<Grid item xs={12} sm={6} md={3} className={classes.gridItem}>
						<Stack justifyContent={'center'} alignItems={'center'}>
							<List>
								{navElements.map((el) => (
									<ListItem key={el.url} className={classes.listItem}>
										<Link href={el.url} sx={{ color: 'black' }}>
											{el.title}
										</Link>
									</ListItem>
								))}
							</List>
						</Stack>
					</Grid>
					<Grid item xs={12} sm={6} md={3} className={classes.gridItem}>
						<Stack justifyContent={'center'} alignItems={'center'}>
							<List>
								{additionalNavElements.map((el) => (
									<ListItem key={el.url} className={classes.listItem}>
										<Link data-t href={el.url} sx={{ color: 'black' }}>
											{el.title}
										</Link>
									</ListItem>
								))}
							</List>
						</Stack>
					</Grid>
					<Grid
						item
						xs={12}
						sm={6}
						md={3}
						flexDirection={'column'}
						justifyContent={'center'}
						alignItems={'center'}
						className={classes.gridItem}
						sx={{
							textAlign: 'center',
							color: 'black',
							display: 'flex',
						}}>
						<Typography
							variant='body2'
							sx={{ marginTop: '2px', marginBottom: '2px' }}>
							Мы на связи
						</Typography>
						<Typography
							variant='body2'
							sx={{ marginTop: '2px', marginBottom: '2px' }}>
							dogfood.ru@gmail.com
						</Typography>
						<Typography
							variant='body2'
							sx={{ marginTop: '2px', marginBottom: '2px' }}>
							8 (999) 00-00-00
						</Typography>
						<Stack
							direction='row'
							sx={{ marginTop: '2px', marginBottom: '2px' }}>
							{socialMedia.map((el) => (
								<Link
									key={el.url}
									href={el.url}
									sx={{
										color: 'black',
										padding: '2px',
										margin: '2px',
										boxShadow: 'inset 0 4px 6px rgba(99,95,99,1)',
										borderRadius: '50px',
									}}>
									{<el.type />}
								</Link>
							))}
						</Stack>
					</Grid>
				</Grid>
				<Grid
					container
					direction={'column'}
					justifyContent={'space-between'}
					alignItems={'center'}
					sx={{
						flexGrow: 1,
						display: {
							xs: 'flex',
							md: 'none',
							textAlign: 'center',
							color: 'black',
						},
					}}
					className={classes.gridContainer}>
					<Typography variant='h6' sx={{ marginTop: '2px' }}>
						Мы на связи
					</Typography>
					<Typography variant='h5' sx={{ marginTop: '2px' }}>
						8 (999) 00-00-00
					</Typography>
					<Typography variant='body2'>dogfood.ru@gmail.com</Typography>
					<Stack direction='row' sx={{ marginTop: '2px', marginBottom: '2px' }}>
						{socialMedia.map((el) => (
							<Link
								key={el.url}
								href={el.url}
								sx={{
									color: 'black',
									padding: '2px',
									margin: '2px',
									boxShadow: 'inset 0 4px 6px rgba(99,95,99,1)',
									borderRadius: '50px',
								}}>
								{<el.type />}
							</Link>
						))}
					</Stack>
					<Typography
						variant='body2'
						sx={{ marginTop: '2px', marginBottom: '2px' }}>
						© «Интернет-магазин DogFood.ru»
					</Typography>
					<Stack
						spacing={1}
						direction='row'
						justifyContent={'center'}
						alignItems={'center'}
						sx={{ marginTop: '2px', marginBottom: '2px' }}>
						<IconWithDescription type='facebook' description='Звезда' />
						<IconWithDescription type='facebook' description='Звезда' />
						<IconWithDescription type='facebook' description='Звезда' />
						<IconWithDescription type='facebook' description='Звезда' />
						<IconWithDescription type='facebook' description='Звезда' />
					</Stack>
				</Grid>
			</Toolbar>
		</AppBar>
	);
};
