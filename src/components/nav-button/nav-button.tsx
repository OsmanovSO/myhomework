import { Button, SxProps } from '@mui/material';
import { FC } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { ArrowBackIos } from '@mui/icons-material';

const DEFAULT_TEXT = 'Back';

type TNavBackProps = {
	text?: string;
	sx?: SxProps;
};

const NavButton: FC<TNavBackProps> = ({ text = DEFAULT_TEXT, sx }) => {
	const DEFAULT_ROUTE = '/';

	const { state } = useLocation();
	return (
		<Button
			component={Link}
			to={state?.from || DEFAULT_ROUTE}
			startIcon={<ArrowBackIos />}
			sx={sx}>
			{text}
		</Button>
	);
};

export default NavButton;
