import { FC } from 'react';
import { Box, SxProps, Typography } from '@mui/material';

type TPriceProps = {
	price: number;
	discount?: number;
	sx?: SxProps;
};

export const Price: FC<TPriceProps> = ({ price, discount, sx }) => {
	return (
		<Box sx={sx}>
			{discount ? (
				<>
					<Typography
						variant='subtitle2'
						sx={{ textDecoration: 'line-through' }}>
						{price + discount} ₽
					</Typography>
					<Typography variant='h6' color='error'>
						{price} ₽
					</Typography>
				</>
			) : (
				<Typography variant='h6'>{price} ₽</Typography>
			)}
		</Box>
	);
};
