import React, { FC, useEffect, useState } from 'react';
import { alpha, InputBase, styled } from '@mui/material';
import { useSearchParams } from 'react-router-dom';
import { useAppDispatch } from '../../store/hooks';
import { setHeaderSearchQuery } from '../../store/reducers/root-layout/root-layout-slice';

const StyledInputBase = styled(InputBase)(({ theme }) => ({
	position: 'relative',
	borderRadius: 50,
	color: 'black',
	backgroundColor: alpha(theme.palette.common.white, 0.95),
	'&:hover': {
		backgroundColor: theme.palette.common.white,
	},
	marginRight: theme.spacing(2),
	marginLeft: 0,
	width: '50%',
	[theme.breakpoints.up('xl')]: {
		width: '50%',
	},
	'& .MuiInputBase-input': {
		padding: theme.spacing(1, 1, 1, 0),
		// vertical padding + font size from searchIcon
		paddingLeft: `calc(1em + ${theme.spacing(4)})`,
		paddingRight: `calc(1em + ${theme.spacing(4)})`,

		transition: theme.transitions.create('width'),
	},
}));

export const Search: FC = () => {
	const SEARCH_PARAM = 'q';
	const dispatch = useAppDispatch();
	const [searchParams, setSearchParams] = useSearchParams();

	const [query, setQuery] = useState(() => {
		return searchParams.get(SEARCH_PARAM) ?? '';
	});

	useEffect(() => {
		if (query) {
			searchParams.set(SEARCH_PARAM, query);
			dispatch(setHeaderSearchQuery(query));
		} else {
			searchParams.delete(SEARCH_PARAM);
			dispatch(setHeaderSearchQuery(''));
		}
		setSearchParams(searchParams);
	}, [query, dispatch, searchParams, setSearchParams]);

	return (
		<>
			<StyledInputBase
				value={query}
				placeholder='Search…'
				inputProps={{ 'aria-label': 'search' }}
				onChange={(e) => setQuery(e.target.value)}
			/>
		</>
	);
};
