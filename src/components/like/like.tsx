import { ChangeEvent, useCallback, useState } from 'react';
import { IconButton } from '@mui/material';
import { Favorite, FavoriteBorder } from '@mui/icons-material';
import { useLikeProductMutation } from '../../store/api/products-api';

type LikeType = {
	isLike: boolean;
	productId: string;
};

export const Like: React.FC<LikeType> = ({ isLike, productId }) => {
	const [likeState, setLikeState] = useState<boolean>(isLike);
	const [likePostRequest] = useLikeProductMutation();
	const onClick = useCallback(
		(event: ChangeEvent<unknown>) => {
			event.preventDefault();
			setLikeState((isLike) => {
				likePostRequest({ id: productId, isFavorite: !isLike });
				return !isLike;
			});
		},
		[productId, likePostRequest]
	);

	return (
		<IconButton onClick={onClick}>
			<>
				{likeState ? (
					<Favorite sx={{ color: 'error.main' }} />
				) : (
					<FavoriteBorder sx={{ color: 'text.primary' }} />
				)}
			</>
		</IconButton>
	);
};
