import React, { useState } from 'react';
import {
	Button,
	ButtonBase,
	Grid,
	IconButton,
	Paper,
	Rating,
	Stack,
	Typography,
} from '@mui/material';
import styled from '@mui/styles/styled';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import DeleteIcon from '@mui/icons-material/Delete';
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import { useNavigate, useParams } from 'react-router-dom';
import WorkspacePremiumIcon from '@mui/icons-material/WorkspacePremium';
import { toast } from 'react-toastify';
import Reviews from '../reviews';
import {
	useDeleteProductMutation,
	useFetchProductQuery,
} from '../../store/api/products-api';
import { withProtection } from '../../HOCs/with-protection';
import Like from '../like';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { selectUser } from '../../store/reducers/root-layout/selectors';
import { selectProductCount } from '../../store/reducers/basket/selectors';
import {
	productDec,
	productInc,
} from '../../store/reducers/basket/basketSlice';
import NavButton from '../nav-button/nav-button';
import { getMessageFromError } from '../../utils/errorUtil';

const Img = styled('img')({
	margin: 'auto',
	display: 'block',
	maxWidth: '100%',
	maxHeight: '100%',
});

export const ProductPage = withProtection(() => {
	const { id } = useParams();

	const navigate = useNavigate();

	const { data } = useFetchProductQuery(id || '', { skip: !id });

	const user = useAppSelector(selectUser);

	const dispatch = useAppDispatch();
	const count = useAppSelector(selectProductCount(String(id)));

	const inc = () => {
		dispatch(productInc(String(id)));
	};

	const dec = () => {
		dispatch(productDec(String(id)));
	};

	const [deleteProductMutation] = useDeleteProductMutation();

	const deleteProduct = async () => {
		try {
			await deleteProductMutation(id || '').unwrap();
			toast.success('Продукт удален');
			navigate('products');
		} catch (e) {
			toast.error(getMessageFromError(e, 'Неожиданная ошибка'));
		}
	};

	const [value, setValue] = useState<number | null>(2);

	const buttonGroupStyle = {
		border: '1px solid #000',
		borderRadius: 25,
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between',
		marginRight: '10px',
	};

	return (
		<Paper
			sx={{
				p: '0 200px',
				marginTop: 10,
				maxWidth: 'auto',
				flexGrow: 1,
				backgroundColor: (theme) =>
					theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
			}}>
			{data && (
				<Grid container spacing={2}>
					<Grid item xs container direction='column' spacing={2}>
						<Grid item xs container direction='column'>
							<Grid item>
								<NavButton sx={{ mb: 2 }} />
								<Typography variant='h5'>{data?.name}</Typography>
							</Grid>
							<Grid item container spacing={2}>
								<Grid item>
									<Typography variant='body2'>Артикул</Typography>
								</Grid>
								<Grid item>
									<Rating
										name='simple-controlled'
										value={value}
										onChange={(event, newValue) => {
											setValue(newValue);
										}}
									/>
								</Grid>
								<Grid item>
									<Typography variant='body2'>отзыв</Typography>
								</Grid>
							</Grid>
						</Grid>
						<Grid container>
							<Grid item xs={8} sx={{ display: 'flex' }}>
								<ButtonBase sx={{ width: 1000, height: 500 }}>
									<Img alt='complex' src={data.pictures} />
								</ButtonBase>
							</Grid>
							<Grid item xs={1} sx={{ display: 'flex' }}>
								<ButtonBase sx={{ width: 50, height: 50 }}>
									<Img alt='complex' src={data.pictures} />
								</ButtonBase>
							</Grid>
							<Grid item direction='column' xs={3}>
								<Grid item>
									<Button
										onClick={deleteProduct}
										sx={{ mb: 2 }}
										startIcon={<DeleteIcon />}>
										Удалить
									</Button>
								</Grid>
								<Grid
									item
									sx={{
										display: 'flex',
									}}>
									{count > 0 ? (
										<Grid item xs={3} sx={buttonGroupStyle}>
											<IconButton
												aria-label='Уменьшить'
												sx={{
													padding: '0px',
													margin: '0px',
												}}
												onClick={dec}>
												<RemoveIcon />
											</IconButton>
											<Typography variant='body1'>{count}</Typography>
											<IconButton
												aria-label='Увеличить'
												sx={{
													padding: '0px',
													margin: '0px',
												}}
												onClick={inc}>
												<AddIcon />
											</IconButton>
										</Grid>
									) : (
										<Grid item xs={8}>
											<Button
												sx={{
													color: 'black',
													backgroundColor: '#FFE44D',
													borderRadius: 25,
													width: '90%',
												}}
												variant='contained'
												onClick={inc}>
												В корзину
											</Button>
										</Grid>
									)}
								</Grid>
								<Grid item>
									<Button
										startIcon={
											<Like
												isLike={data.likes.includes(user?.id as string)}
												productId={data.id}
											/>
										}>
										В избранное
									</Button>
								</Grid>
								<Grid
									xs={6}
									maxWidth={400}
									item
									wrap='nowrap'
									container
									spacing={2}>
									<Grid item>
										<LocalShippingIcon />
									</Grid>
									<Grid item>
										<Typography variant='h5' component='div'>
											Доставка по всему Миру!
										</Typography>
										<Typography paragraph>
											Доставка курьером — от 399 ₽
										</Typography>
										<Typography paragraph>
											Доставка в пункт выдачи — от 199 ₽{' '}
										</Typography>
									</Grid>
								</Grid>
								<Grid
									xs={6}
									maxWidth={400}
									item
									wrap='nowrap'
									container
									spacing={2}>
									<Grid item>
										<WorkspacePremiumIcon />
									</Grid>
									<Grid item>
										<Typography variant='h5' component='div'>
											Гарантия качества
										</Typography>
										<Typography paragraph>
											Если Вам не понравилось качество нашей продукции, мы
											вернем деньги, либо сделаем все возможное, чтобы
											удовлетворить ваши нужды.
										</Typography>
									</Grid>
								</Grid>
							</Grid>
						</Grid>
						<Grid item>
							<Typography variant='h5'>Описание</Typography>
							<Typography variant='body2'>{data.description}</Typography>
							<Typography variant='h5' sx={{ mb: 2 }}>
								Отзывы
							</Typography>
							<Stack direction={{ xs: 'column' }}>
								<Reviews reviews={data.reviews} />
							</Stack>
						</Grid>
					</Grid>
				</Grid>
			)}
		</Paper>
	);
});
