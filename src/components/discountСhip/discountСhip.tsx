import { Chip, SxProps } from '@mui/material';
import { FC } from 'react';

type TDiscountChipProps = {
	price: number;
	discount?: number;
	sx?: SxProps;
};

export const DiscountChip: FC<TDiscountChipProps> = ({
	price,
	discount,
	sx,
}) => {
	const discountPercent: number =
		price && discount ? Math.round(100 / (price / discount)) : 0;
	return (
		<>
			{discountPercent > 0 && (
				<Chip label={`${discountPercent}%`} color='error' sx={sx} />
			)}
		</>
	);
};
