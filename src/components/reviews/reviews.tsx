import { Box, Rating, Typography } from '@mui/material';
import React from 'react';
import { TReview } from '../../models/models';
import { withProtection } from '../../HOCs/with-protection';

type ReviewsPropsType = {
	reviews: TReview[];
};

export const Reviews: React.FC<ReviewsPropsType> = withProtection(
	({ reviews }) => {
		return (
			<>
				{reviews.map((review) => {
					const {
						rating,
						text,
						author: { name },
						id,
					} = review;
					return (
						<Box key={id} sx={{ mb: 2 }}>
							<Typography variant='h6'>{name}</Typography>
							<Rating sx={{ mr: 2 }} readOnly value={parseInt(rating)} />
							<Typography variant='body1'>{text}</Typography>
						</Box>
					);
				})}
			</>
		);
	}
);
