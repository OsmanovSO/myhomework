import { FC } from 'react';
import { IconButton, Stack, SxProps, Typography } from '@mui/material';
import { Add, Remove } from '@mui/icons-material';
import Button from '@mui/material/Button';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { selectProductCount } from '../../store/reducers/basket/selectors';
import {
	productDec,
	productInc,
} from '../../store/reducers/basket/basketSlice';

type TQuantityButtonProps = {
	productId: string;
	stock: number;
	sx?: SxProps;
};

export const QuantityButton: FC<TQuantityButtonProps> = ({
	productId,
	stock,
	sx,
}) => {
	const dispatch = useAppDispatch();
	const count = useAppSelector(selectProductCount(productId));

	const inc = () => {
		dispatch(productInc(productId));
	};

	const dec = () => {
		dispatch(productDec(productId));
	};

	return count > 0 ? (
		<Stack direction='row' spacing={1} sx={sx}>
			<IconButton onClick={dec}>
				<Remove />
			</IconButton>
			<Typography
				variant='h6'
				sx={{ display: 'flex', alignItems: 'center', p: 0, m: 0 }}>
				{count}
			</Typography>
			<IconButton disabled={count >= stock} onClick={inc}>
				<Add />
			</IconButton>
		</Stack>
	) : (
		<Button
			disabled={stock < 1}
			onClick={inc}
			sx={{
				color: 'black',
				backgroundColor: '#FFE44D',
				borderRadius: 25,
				width: '90%',
			}}
			variant='contained'>
			В корзину
		</Button>
	);
};
