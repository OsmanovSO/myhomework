import { Route } from 'react-router';
import { Navigate, Routes } from 'react-router-dom';
import { Box } from '@mui/material';
import ProductsPage from '../../pages/products-page';
import ProfilePage from '../../pages/profile-page';
import FavouritesPage from '../../pages/favourites-page';
import SignUpPage from '../../pages/sign-up-page';
import SignInPage from '../../pages/sign-in-page';
import ProductCreatePage from '../../pages/productCreatePage';
import NotFoundPage from '../not-found';
import ProductPage from '../product-page';
import Header from '../header';
import Footer from '../footer';
import './styles.css';
import BasketPage from '../../pages/basketPage/basketPage';

export const App = () => {
	return (
		<Box
			sx={{
				display: 'flex',
				flexDirection: 'column',
				minHeight: '100vh',
			}}>
			<Header />
			<Routes>
				<Route path='/' element={<Navigate to='/products' replace />} />
				<Route path='/products' element={<ProductsPage />} />
				<Route path='/product/:id' element={<ProductPage />} />
				<Route path='/productCreate' element={<ProductCreatePage />} />
				<Route path='/profile' element={<ProfilePage />} />
				<Route path='/favourites' element={<FavouritesPage />} />
				<Route path='/baskets' element={<BasketPage />} />
				<Route path='/signup' element={<SignUpPage />} />
				<Route path='/signin' element={<SignInPage />} />
				<Route path='*' element={<NotFoundPage />} />
			</Routes>
			<Footer />
		</Box>
	);
};
