import { useCallback, useState } from 'react';
import { Container, Grid, Stack } from '@mui/material';
import { useDebounce } from 'use-debounce';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import ProductCard from '../../components/product-card';
import { useAppSelector } from '../../store/hooks';
import { useFetchProductsQuery } from '../../store/api/products-api';
import { selectHeaderSearchQuery } from '../../store/reducers/root-layout/selectors';
import { withProtection } from '../../HOCs/with-protection';
import { LoadMore } from '../../components/load-more/load-more';
import { getMessageFromError } from '../../utils/errorUtil';

const PER_PAGE = 16;

export const ProductsPage = withProtection(() => {
	const query = useAppSelector(selectHeaderSearchQuery);
	const [queryDebounce] = useDebounce(query, 500);
	const [page, setPage] = useState<number>(1);
	const { data, isLoading, isError, isFetching, error, refetch } =
		useFetchProductsQuery({
			page: page,
			limit: PER_PAGE,
			query: queryDebounce || undefined,
		});

	const isEndOfList = data && data.products?.length >= data.total;

	const loadMore = useCallback(() => {
		setPage((prev) => prev + 1);
	}, []);

	return (
		<Container sx={{ flexGrow: 1, marginTop: 10 }}>
			<Button component={Link} to={'/productCreate'} sx={{ mb: 2 }}>
				{'Создать продукт'}
			</Button>
			<Grid container item columns={{ xs: 4, sm: 9, md: 12 }}>
				{data?.products &&
					data?.products.map((item) => (
						<Grid key={item.id} item xs={2} sm={3} md={3}>
							<ProductCard
								key={item.id}
								id={item.id}
								price={item.price}
								discount={item.discount}
								description={item.description}
								name={item.name}
								pictures={item.pictures}
								reviews={item.reviews}
								likes={item.likes}
								stock={item.stock}
								isLoading={isLoading}
								isError={isError}
								error={getMessageFromError(error, 'message.unexpectedError')}
								refetch={refetch}
							/>
						</Grid>
					))}
			</Grid>
			<Stack spacing={2} sx={{ marginTop: 2 }}>
				<LoadMore
					action={loadMore}
					isEndOfList={isEndOfList}
					isLoading={isFetching && page > 2}
				/>
			</Stack>
		</Container>
	);
});
