import { Container, Grid } from '@mui/material';
import { useDebounce } from 'use-debounce';
import { useMemo, useState } from 'react';
import ProductCard from '../../components/product-card';
import { useAppSelector } from '../../store/hooks';
import { useFetchProductsQuery } from '../../store/api/products-api';
import {
	selectHeaderSearchQuery,
	selectUser,
} from '../../store/reducers/root-layout/selectors';
import { withProtection } from '../../HOCs/with-protection';
import { getMessageFromError } from '../../utils/errorUtil';
import NavButton from '../../components/nav-button/nav-button';

export const FavouritesPage = withProtection(() => {
	const query = useAppSelector(selectHeaderSearchQuery);
	const user = useAppSelector(selectUser);

	const [queryDebounce] = useDebounce(query, 500);

	const [page] = useState<number>(1);

	const { data, isLoading, isError, error, refetch } = useFetchProductsQuery({
		page: page,
		limit: 16,
		query: queryDebounce || undefined,
	});
	const favouritesProducts = useMemo(() => {
		if (!user || !data || !data.products) {
			return [];
		}
		return data.products.filter((product) => {
			return product.likes.includes(user.id);
		});
	}, [user, data]);

	return (
		<Container sx={{ flexGrow: 1, marginTop: 10 }}>
			<NavButton sx={{ mb: 2 }} />
			<Grid container item columns={{ xs: 4, sm: 9, md: 12 }}>
				{favouritesProducts &&
					favouritesProducts.map((product) => (
						<Grid key={product.id} item xs={2} sm={3} md={3}>
							<ProductCard
								isLoading={isLoading}
								isError={isError}
								error={getMessageFromError(error, 'Непредвиденная ошибка')}
								refetch={refetch}
								{...product}
							/>
						</Grid>
					))}
			</Grid>
		</Container>
	);
});
