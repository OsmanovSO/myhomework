import { FC } from 'react';
import { Button, Container, Grid, Stack, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import { Link } from 'react-router-dom';
import { withProtection } from '../../HOCs/with-protection';
import { selectUser } from '../../store/reducers/root-layout/selectors';
import { useAppSelector } from '../../store/hooks';
import NavButton from '../../components/nav-button/nav-button';

export const ProfilePage: FC = withProtection(() => {
	const user = useAppSelector(selectUser);
	return (
		<>
			<Container maxWidth='lg' sx={{ flexGrow: 1, marginTop: 10 }}>
				<Stack direction='row'>
					<NavButton sx={{ mb: 2 }} />
					<Box sx={{ flexGrow: 1 }} />
					<Button component={Link} to={'/profile-edit'} sx={{ mb: 2 }}>
						{'Редактировать профиль'}
					</Button>
				</Stack>
				{user && (
					<Grid container spacing={2}>
						<Grid item xs={12} lg={6}>
							<img width={'100%'} src={user.avatar} alt='TODO' loading='lazy' />
						</Grid>
						<Grid item xs={12} lg={6}>
							<Stack direction={'column'} spacing={2}>
								<Typography variant='h4'>{user.name}</Typography>
								<Typography variant='body1'>{user.email}</Typography>
								<Typography variant='body1'>{user.group}</Typography>
								<Typography variant='body1'>{user.about}</Typography>
							</Stack>
						</Grid>
					</Grid>
				)}
			</Container>
		</>
	);
});
