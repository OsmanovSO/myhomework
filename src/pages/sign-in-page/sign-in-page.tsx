import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import * as yup from 'yup';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import LoadingButton from '@mui/lab/LoadingButton';
import { Button, Stack } from '@mui/material';
import { getMessageFromError } from '../../utils/errorUtil';
import {
	setTokens,
	setUser,
} from '../../store/reducers/root-layout/root-layout-slice';
import { useSignInMutation } from '../../store/api/auth-api';
import { useAppDispatch } from '../../store/hooks';

export type TSignInFormValue = {
	email: string;
	password: string;
};

export function SignInPage() {
	const LABEL = 'Sign In';
	const LABEL_EMAIL = 'Email';
	const LABEL_PASSWORD = 'Password';
	const LABEL_SIGN_UP = 'Sign Up';
	const MESSAGE_SUCCESS = 'Welcome';
	const MESSAGE_ERROR_DEFAULT = 'Unexpected error';
	const NAVIGATE_PATH_SUCCESS = '/';
	const NAVIGATE_PATH_SIGN_UP = '/signup';

	const location = useLocation();
	const { state, pathname } = location;
	const navigate = useNavigate();
	const dispatch = useAppDispatch();

	const signInFormSchema = yup.object({
		email: yup.string().email().required().strict(),
		password: yup.string().required().min(6).max(24),
	});

	const {
		control,
		handleSubmit,
		formState: { errors, isValid, isSubmitting, isSubmitted },
	} = useForm<TSignInFormValue>({
		defaultValues: {
			email: '',
			password: '',
		},
		resolver: yupResolver(signInFormSchema),
	});

	const [signInRequestFn] = useSignInMutation();

	const navigateAfterSuccess =
		state?.from && state?.from !== pathname
			? state.from
			: NAVIGATE_PATH_SUCCESS;

	const submitHandler: SubmitHandler<TSignInFormValue> = async (values) => {
		try {
			const response = await signInRequestFn(values).unwrap();
			dispatch(setTokens({ accessToken: response.token, refreshToken: '' }));
			dispatch(setUser(response.data));
			toast.success(MESSAGE_SUCCESS);
			navigate(navigateAfterSuccess, {
				state: { from: location.pathname },
			});
		} catch (e) {
			toast.error(getMessageFromError(e, MESSAGE_ERROR_DEFAULT));
		}
	};

	return (
		<Box
			sx={{
				marginY: 15,
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
			}}>
			<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
				<LockOutlinedIcon />
			</Avatar>
			<Typography component='h1' variant='h5'>
				{LABEL}
			</Typography>
			<Box
				component='form'
				onSubmit={handleSubmit(submitHandler)}
				noValidate
				sx={{ mt: 1, maxWidth: '30rem' }}>
				<Controller
					name={'email'}
					control={control}
					render={({ field }) => (
						<TextField
							margin='normal'
							required
							fullWidth
							label={LABEL_EMAIL}
							autoComplete='email'
							error={!!errors.email?.message}
							helperText={errors.email?.message}
							{...field}
						/>
					)}
				/>
				<Controller
					name={'password'}
					control={control}
					render={({ field }) => (
						<TextField
							type='password'
							margin='normal'
							required
							fullWidth
							label={LABEL_PASSWORD}
							autoComplete='password'
							error={!!errors.password?.message}
							helperText={errors.password?.message}
							{...field}
						/>
					)}
				/>
				<LoadingButton
					type='submit'
					disabled={isSubmitted && (!isValid || isSubmitting)}
					loading={isSubmitting}
					fullWidth
					variant='contained'
					sx={{ mt: 3, mb: 2 }}>
					{LABEL}
				</LoadingButton>
				<Stack direction='row' justifyContent='center' alignItems='center'>
					<Button
						component={RouterLink}
						to={NAVIGATE_PATH_SIGN_UP}
						state={{ from: location.pathname }}>
						{LABEL_SIGN_UP}
					</Button>
				</Stack>
			</Box>
		</Box>
	);
}
