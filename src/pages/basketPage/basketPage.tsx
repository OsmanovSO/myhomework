import { ChangeEvent, FC, useEffect, useMemo, useState } from 'react';
import {
	Button,
	Checkbox,
	Container,
	FormControlLabel,
	Grid,
	Paper,
	Stack,
} from '@mui/material';
import Typography from '@mui/material/Typography';
import { useDebounce } from 'use-debounce';
import Box from '@mui/material/Box';
import { useFetchCartProductsQuery } from '../../store/api/products-api';
import Price from '../../components/price';
import { withProtection } from '../../HOCs/with-protection';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import {
	selectBasket,
	selectBasketCount,
} from '../../store/reducers/basket/selectors';
import { TProduct } from '../../models/models';
import { selectHeaderSearchQuery } from '../../store/reducers/root-layout/selectors';
import {
	productsRemove,
	TBasket,
} from '../../store/reducers/basket/basketSlice';
import NavButton from '../../components/nav-button/nav-button';
import { BasketProduct } from '../../components/basketProduct/basketProduct';

type TProductsInCartData = {
	products: TProduct[];
	totalPrice: number;
	totalDiscount: number;
};

const getProductsInCartData = (
	products: TProduct[] | null,
	cart: TBasket
): TProductsInCartData => {
	const result: TProductsInCartData = {
		products: [],
		totalPrice: 0,
		totalDiscount: 0,
	};
	(products || []).forEach((product) => {
		const productInCartCount = cart[product.id];
		if (product.id in cart && productInCartCount > 0) {
			result.products.push(product);
			result.totalPrice =
				result.totalPrice + product.price * productInCartCount;
			result.totalDiscount =
				result.totalDiscount + product.discount * productInCartCount;
		}
	});
	return result;
};

const BasketPage: FC = () => {
	const dispatch = useAppDispatch();
	const basket = useAppSelector(selectBasket);
	const cartCount = useAppSelector(selectBasketCount);
	const query = useAppSelector(selectHeaderSearchQuery);
	const [debounceQuery] = useDebounce(query, 500);
	const { data } = useFetchCartProductsQuery(debounceQuery || undefined);
	const { products, totalPrice, totalDiscount } = useMemo(
		() => getProductsInCartData(data?.products || null, basket),
		[basket, data]
	);
	const [selected, setSelected] = useState<string[]>([]);
	const [selectedAll, setSelectedAll] = useState<boolean>(false);

	useEffect(() => {
		setSelectedAll(products.length === selected.length);
	}, [selected, products]);

	const onSelectAll = (e: ChangeEvent<HTMLInputElement>) => {
		setSelected(e.target.checked ? products.map((product) => product.id) : []);
	};

	const onProductsRemove = () => {
		dispatch(productsRemove(selected));
	};

	return (
		<Container sx={{ flexGrow: 1, marginTop: 10 }}>
			<NavButton sx={{ mb: 2 }} />
			<Grid container spacing={2}>
				<Grid item xs={12} lg={8}>
					<Paper sx={{ mb: 2, display: 'flex' }}>
						<FormControlLabel
							control={
								<Checkbox checked={selectedAll} onChange={onSelectAll} />
							}
							label={'Выбрать все'}
							sx={{ m: 1 }}
						/>
						<Box sx={{ flexGrow: 1 }} />
						{selected.length > 0 && (
							<Button onClick={onProductsRemove} sx={{ m: 1 }}>
								Удалить выбранные
							</Button>
						)}
					</Paper>
					{products && (
						<Stack direction={'column'} spacing={2}>
							{products.map((product) => (
								<BasketProduct key={product.id} product={product} />
							))}
						</Stack>
					)}
				</Grid>
				<Grid item xs={12} lg={4}>
					<Paper sx={{ p: 2 }}>
						<Typography variant='subtitle2'>В общем</Typography>
						<Stack direction={'row'} spacing={2} sx={{ display: 'flex' }}>
							<Typography variant='h6'>{`${cartCount} шт`}</Typography>
							<Box sx={{ flexGrow: 1 }} />
							<Price price={totalPrice} discount={totalDiscount} />
						</Stack>
					</Paper>
				</Grid>
			</Grid>
		</Container>
	);
};

export default withProtection(BasketPage);
