import * as yup from 'yup';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import LoadingButton from '@mui/lab/LoadingButton';
import { Button, Stack } from '@mui/material';
import { getMessageFromError } from '../../utils/errorUtil';
import { useSignUpMutation } from '../../store/api/auth-api';

export type TSignUpFormValue = {
	email: string;
	group: string;
	password: string;
};

export function SignUpPage() {
	const navigate = useNavigate();
	const [signUpMutation] = useSignUpMutation();

	const signUpFormSchema = yup.object({
		email: yup.string().email().required().strict(),
		group: yup.string().lowercase().required().strict(),
		password: yup.string().required().min(6).max(24),
	});

	const {
		control,
		handleSubmit,
		formState: { errors, isValid, isSubmitting, isSubmitted },
	} = useForm<TSignUpFormValue>({
		defaultValues: {
			email: '',
			group: '',
			password: '',
		},
		resolver: yupResolver(signUpFormSchema),
	});

	const submitHandler: SubmitHandler<TSignUpFormValue> = async (values) => {
		try {
			await signUpMutation(values).unwrap();
			toast.success('Please sign in');
			navigate('/signin');
		} catch (e) {
			toast.error(getMessageFromError(e, 'Unexpected error'));
		}
	};

	return (
		<Box
			sx={{
				marginY: 15,
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
			}}>
			<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
				<LockOutlinedIcon />
			</Avatar>
			<Typography component='h1' variant='h5'>
				{'Sign Up'}
			</Typography>
			<Box
				component='form'
				onSubmit={handleSubmit(submitHandler)}
				noValidate
				sx={{ mt: 1, maxWidth: '30rem' }}>
				<Controller
					name={'email'}
					control={control}
					render={({ field }) => (
						<TextField
							margin='normal'
							required
							fullWidth
							label={'Email'}
							autoComplete='email'
							error={!!errors.email?.message}
							helperText={errors.email?.message}
							{...field}
						/>
					)}
				/>
				<Controller
					name={'group'}
					control={control}
					render={({ field }) => (
						<TextField
							margin='normal'
							required
							fullWidth
							label={'Group'}
							autoComplete='group'
							error={!!errors.group?.message}
							helperText={errors.group?.message}
							{...field}
						/>
					)}
				/>
				<Controller
					name={'password'}
					control={control}
					render={({ field }) => (
						<TextField
							type='password'
							margin='normal'
							required
							fullWidth
							label={'Password'}
							autoComplete='password'
							error={!!errors.password?.message}
							helperText={errors.password?.message}
							{...field}
						/>
					)}
				/>
				<LoadingButton
					type='submit'
					disabled={isSubmitted && (!isValid || isSubmitting)}
					loading={isSubmitting}
					fullWidth
					variant='contained'
					sx={{ mt: 3, mb: 2 }}>
					{'Sign Up'}
				</LoadingButton>
				<Stack direction='row' justifyContent='center' alignItems='center'>
					<Button
						component={RouterLink}
						to={'/signin'}
						state={{ from: location.pathname }}>
						{'Sign In'}
					</Button>
				</Stack>
			</Box>
		</Box>
	);
}
