import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import * as yup from 'yup';
import { SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';
import { Container, Stack } from '@mui/material';
import { getMessageFromError } from '../../utils/errorUtil';
import { useCreateProductMutation } from '../../store/api/products-api';
import { withProtection } from '../../HOCs/with-protection';
import NavButton from '../../components/nav-button/nav-button';
import ProductCreate from '../../components/productCreate';
import { TProductCreateFormValue } from '../../components/productCreate/productCreate';

export const productFormSchema = yup.object({
	price: yup.number().integer().positive().required(),
	discount: yup.number().integer().positive().required(),
	stock: yup.number().integer().positive().required(),
	description: yup.string().required().min(4).max(200),
	name: yup.string().required().min(4).max(200),
	pictures: yup.string().required().min(4).max(200),
});

export const ProductCreatePage: FC = withProtection(() => {
	const navigateSuccessPath = (productId: string): string =>
		`/product/${productId}`;
	const navigate = useNavigate();
	const [createProductMutation] = useCreateProductMutation();

	const {
		control,
		handleSubmit,
		formState: { errors, isValid, isSubmitting, isSubmitted },
	} = useForm<TProductCreateFormValue>({
		defaultValues: {
			price: 0,
			discount: 0,
			stock: 0,
			description: '',
			name: '',
			pictures: '',
		},
		resolver: yupResolver(productFormSchema),
	});

	const submitHandler: SubmitHandler<TProductCreateFormValue> = async (
		values
	) => {
		try {
			const response = await createProductMutation(values).unwrap();
			toast.success('Изменения сохранены');
			navigate(navigateSuccessPath(response.id), {
				state: { from: location.pathname },
			});
		} catch (e) {
			toast.error(getMessageFromError(e, 'Неожиданная ошибка'));
		}
	};

	return (
		<>
			<Container maxWidth='lg' sx={{ flexGrow: 1, marginTop: 10 }}>
				<Stack direction='row'>
					<NavButton sx={{ mb: 2 }} />
				</Stack>
				<ProductCreate
					onSubmit={handleSubmit(submitHandler)}
					control={control}
					errors={errors}
					isValid={isValid}
					isSubmitting={isSubmitting}
					isSubmitted={isSubmitted}
				/>
			</Container>
		</>
	);
});
