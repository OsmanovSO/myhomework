import { useEffect, useState } from 'react';

export function useEditMode(isEditModeInit: boolean) {
	const BUTTON_EDIT = 'Редактировать';
	const BUTTON_SAVE = 'Сохранить';

	const [isEditMode, setIsEditMode] = useState<boolean>(isEditModeInit);
	const [buttonText, setButtonText] = useState<string>(
		isEditMode ? BUTTON_SAVE : BUTTON_EDIT
	);

	useEffect(() => {
		setButtonText(isEditMode ? BUTTON_SAVE : BUTTON_EDIT);
	}, [isEditMode]);

	return {
		buttonText,
		isEditMode,
		setIsEditMode,
	};
}
