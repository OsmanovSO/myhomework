import { createRoot } from 'react-dom/client';
import { StrictMode } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { App } from './components/app';
import store, { persistor } from './store/store';
import 'react-toastify/dist/ReactToastify.css';
import './index.css';

const domNode = document.getElementById('root') as HTMLDivElement;
const root = createRoot(domNode);
root.render(
	<StrictMode>
		<PersistGate loading={null} persistor={persistor}>
			<Provider store={store}>
				<BrowserRouter>
					<App />
					<ToastContainer theme={'colored'} position={'top-right'} />
				</BrowserRouter>
			</Provider>
		</PersistGate>
	</StrictMode>
);
