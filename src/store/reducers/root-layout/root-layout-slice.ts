import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TUser } from '../../../models/models';

export const sliceName = 'rootLayout';

export type TAuthTokens = {
	accessToken: string;
	refreshToken: string;
};

type TRootLayoutState = {
	tokens: TAuthTokens;
	user: TUser | null;
	headerSearchQuery: string;
};

const initialState: TRootLayoutState = {
	tokens: {
		accessToken: '',
		refreshToken: '',
	},
	user: null,
	headerSearchQuery: '',
};

export const rootLayoutSlice = createSlice({
	name: sliceName,
	initialState,
	reducers: {
		setTokens: (
			store: TRootLayoutState,
			action: PayloadAction<TAuthTokens>
		) => {
			store.tokens = action.payload;
		},
		setUser: (store: TRootLayoutState, action: PayloadAction<TUser>) => {
			store.user = action.payload;
		},
		setHeaderSearchQuery: (
			store: TRootLayoutState,
			action: PayloadAction<string>
		) => {
			store.headerSearchQuery = action.payload;
		},
	},
});

export const { setTokens, setUser, setHeaderSearchQuery } =
	rootLayoutSlice.actions;
export default rootLayoutSlice.reducer;
