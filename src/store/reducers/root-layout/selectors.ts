import { rootLayoutSlice } from './root-layout-slice';
import { RootState } from '../../types';

export const selectTokens = (state: RootState) => {
	return state[rootLayoutSlice.name].tokens;
};

export const selectUser = (state: RootState) => {
	return state[rootLayoutSlice.name].user;
};

export const selectHeaderSearchQuery = (state: RootState) => {
	return state[rootLayoutSlice.name].headerSearchQuery;
};
