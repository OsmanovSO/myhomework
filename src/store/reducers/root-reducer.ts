import storage from 'redux-persist/lib/storage';
import { combineReducers } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';
import { authApi } from '../api/auth-api';
import { rootLayoutSlice } from './root-layout/root-layout-slice';
import { productsApi } from '../api/products-api';
import basketReducer, { basketSlice } from './basket/basketSlice';

const persistConfig = {
	key: 'root',
	storage,
	version: 1,
	blacklist: [authApi.reducerPath, productsApi.reducerPath],
};

export const rootReducer = combineReducers({
	[rootLayoutSlice.name]: rootLayoutSlice.reducer,
	[basketSlice.name]: basketReducer,
	[authApi.reducerPath]: authApi.reducer,
	[productsApi.reducerPath]: productsApi.reducer,
});

export const persistedReducer = persistReducer(persistConfig, rootReducer);
