import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export const sliceName = 'basket';

export type TBasket = {
	[key: string]: number;
};

const initialState: TBasket = {};

export const basketSlice = createSlice({
	name: sliceName,
	initialState,
	reducers: {
		productInc: (store: TBasket, action: PayloadAction<string>) => {
			store[action.payload] = (store[action.payload] || 0) + 1;
		},
		productDec: (store: TBasket, action: PayloadAction<string>) => {
			store[action.payload] = (store[action.payload] || 0) - 1;
		},
		productsRemove: (store: TBasket, action: PayloadAction<string[]>) => {
			action.payload.forEach((productId) => {
				productId in store && delete store[productId];
			});
		},
	},
});

export const { productInc, productDec, productsRemove } = basketSlice.actions;
export default basketSlice.reducer;
