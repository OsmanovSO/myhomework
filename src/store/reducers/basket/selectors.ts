import { RootState } from '../../types';
import { basketSlice } from './basketSlice';

export const selectBasket = (state: RootState) => {
	return state[basketSlice.name];
};

export const selectProductCount = (productId: string) => {
	return (state: RootState) => {
		return state[basketSlice.name][productId];
	};
};

export const selectBasketCount = (state: RootState) => {
	const basket = selectBasket(state);
	return Object.keys(basket).reduce((total, key) => total + basket[key], 0);
};
