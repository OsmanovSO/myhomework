import {
	Control,
	DeepRequired,
	FieldErrorsImpl,
	FieldValues,
} from 'react-hook-form';
import { FormEvent } from 'react';
import store from './store';
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export type TMutationProps<T extends FieldValues> = {
	onSubmit: (e: FormEvent<HTMLFormElement>) => void;
	control: Control<T>;
	errors: Partial<FieldErrorsImpl<DeepRequired<T>>>;
	isValid: boolean;
	isSubmitting: boolean;
	isSubmitted: boolean;
};
