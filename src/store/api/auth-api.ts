import { createApi } from '@reduxjs/toolkit/dist/query/react';
import { TUser } from '../../models/models';
import { customBaseQuery } from './config';
import { TSignUpFormValue } from '../../pages/sign-up-page/sign-up-page';
import { TSignInFormValue } from '../../pages/sign-in-page/sign-in-page';

type TSignInResponse = {
	data: TUser;
	token: string;
};

type BE_TUser = Omit<TUser, 'id'> & {
	_id: TUser['id'];
};

type BE_TSignInResponse = Omit<TSignInResponse, 'data'> & {
	data: BE_TUser;
};

function modifyUser(BE_User: BE_TUser) {
	const { _id, ...rest } = BE_User;
	return { id: _id, ...rest };
}

export const authApi = createApi({
	reducerPath: 'authApi',
	baseQuery: customBaseQuery,
	endpoints: (builder) => ({
		signUp: builder.mutation<TUser, TSignUpFormValue>({
			query: (signUpFormValues) => ({
				url: 'signup',
				method: 'POST',
				body: signUpFormValues,
			}),
		}),
		signIn: builder.mutation<TSignInResponse, TSignInFormValue>({
			query: (signInFormValues) => ({
				url: 'signin',
				method: 'POST',
				body: signInFormValues,
			}),
			transformResponse: (BE_Data: BE_TSignInResponse) => {
				const { data, ...rest } = BE_Data;

				return {
					data: modifyUser(data),
					...rest,
				};
			},
		}),
	}),
});

export const { useSignUpMutation, useSignInMutation } = authApi;
