import { createApi } from '@reduxjs/toolkit/query/react';
import { customBaseQuery } from './config';
import { TProduct } from '../../models/models';
import { TProductCreateFormValue } from '../../components/productCreate/productCreate';

export type TProductsFilters = {
	page?: number;
	limit?: number;
	query?: string;
	id?: string;
};

export type TProductsResponse = {
	products: TProduct[];
	total: number;
};

type BE_TProduct = Omit<TProduct, 'id'> & {
	_id: TProduct['id'];
};

type BE_TProductsResponse = Omit<TProductsResponse, 'products'> & {
	products: BE_TProduct[];
};

function modifyProduct(BE_Product: BE_TProduct) {
	const { _id, ...rest } = BE_Product;
	return { id: _id, ...rest };
}

export const productsApi = createApi({
	reducerPath: 'productsApi',
	baseQuery: customBaseQuery,
	tagTypes: ['Products', 'Product', 'BasketProducts'],
	endpoints: (builder) => ({
		fetchProducts: builder.query<TProductsResponse, TProductsFilters>({
			query: ({ page, limit, query }) => ({
				url: 'products',
				params: {
					page,
					limit,
					query,
				},
			}),
			providesTags: (response) => {
				return response
					? [
							...response.products.map(({ id }) => ({
								type: 'Products' as const,
								id,
							})),
							{ type: 'Products', id: 'PRODUCT_LIST' },
					  ]
					: [{ type: 'Products', id: 'PRODUCT_LIST' }];
			},
			transformResponse: (BE_Data: BE_TProductsResponse) => {
				const { products, ...rest } = BE_Data;
				return { products: products.map(modifyProduct), ...rest };
			},
			serializeQueryArgs: ({ endpointName, queryArgs: { query } }) => {
				return endpointName + query;
			},
			merge: (currentCache, newValue, { arg: { page } }) => {
				if (page === 1) return;
				currentCache.products.push(...newValue.products);
			},
			forceRefetch({ currentArg, previousArg }) {
				return currentArg !== previousArg;
			},
		}),
		fetchCartProducts: builder.query<TProductsResponse, string | undefined>({
			query: (query) => ({
				url: 'products',
				params: {
					query,
				},
			}),
			transformResponse: (BE_Data: BE_TProductsResponse) => {
				const { products, ...rest } = BE_Data;
				return { products: products.map(modifyProduct), ...rest };
			},
			providesTags: ['BasketProducts'],
		}),
		fetchProduct: builder.query<TProduct, string>({
			query: (id) =>
				id && {
					url: `products/${id}`,
				},
			transformResponse: modifyProduct,
			providesTags: (response) => [{ type: 'Product', id: response?.id }],
		}),
		createProduct: builder.mutation<TProduct, TProductCreateFormValue>({
			query: (productFormValue) => ({
				url: 'products',
				method: 'POST',
				body: productFormValue,
			}),
			invalidatesTags: [{ type: 'Products', id: 'PRODUCT_LIST' }],
			transformResponse: modifyProduct,
		}),
		deleteProduct: builder.mutation<TProduct, string>({
			query: (id) => ({
				url: `products/${id}`,
				method: 'DELETE',
			}),
			invalidatesTags: ['Products'],
		}),
		likeProduct: builder.mutation<
			TProduct,
			{ id: string; isFavorite: boolean }
		>({
			query: ({ id, isFavorite }) => ({
				url: `products/likes/${id}`,
				method: isFavorite ? 'PUT' : 'DELETE',
			}),
			invalidatesTags: [
				{ type: 'Products', id: 'PRODUCT_LIST' },
				'BasketProducts',
				'Product',
			],
			async onQueryStarted(_, { dispatch, queryFulfilled }) {
				try {
					const { data: updatedProduct } = await queryFulfilled;
					dispatch(
						productsApi.util.updateQueryData('fetchProducts', _, (draft) => {
							const productIndex = draft.products.findIndex(
								(product) => updatedProduct.id === product.id
							);
							productIndex !== -1 &&
								(draft.products[productIndex] = updatedProduct);
						})
					);
				} catch {}
			},
			transformResponse: modifyProduct,
		}),
	}),
});

export const {
	useFetchProductsQuery,
	useFetchCartProductsQuery,
	useFetchProductQuery,
	useDeleteProductMutation,
	useLikeProductMutation,
	useCreateProductMutation,
} = productsApi;
